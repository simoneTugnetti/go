// Simone Tugnetti

// Control Flow And Scan

package main

import "fmt"

func main() {
	ifBlock()
	forLoop()
	switchCase()
	scanInput()
}

// If Condition --> Se condizione è vera, entrerà nel blocco,
// altrimenti continuerà in else
func ifBlock() {
	var x = 100

	if x < 100 {
		println("Il valore è minore di 100")
	} else if x > 100 {
		println("Il valore è maggiore di 100")
	} else {
		println("Il valore è 100")
	}
}

// For Loop --> Ciclo che termina se la condizione risulta False
func forLoop() {
	for i := 0; i < 3; i++ {
		println("Prima frase")
	}

	var i = 0
	for i < 2 {
		println("Seconda frase")
		i++
	}

	/*
		for {
			println("Infinite Loop")
		}
	*/

}

// Switch --> entra in un caso in base al valore all'interno del tag
func switchCase() {
	x := 3
	switch x {
	case 1:
		println("x è 1")
	case 2:
		println("x è 2")
	case 3:
		println("x è 3")
	default:
		println("x è un altro valore")
	}
}

func scanInput() {
	var numApple int
	println("Numero di mele?")

	fmt.Scan(&numApple)

	println("Le mele sono", numApple)
}
