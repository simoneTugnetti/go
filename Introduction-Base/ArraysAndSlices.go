// Simone Tugnetti

// Arrays and Slices

package main

import "fmt"

func main() {
	arrays()
	slices()
}

// Array --> Collezione di dati inizializzati a 0
func arrays() {
	var x [5]int

	x[0] = 2
	println(x[1])

	// Array preimpostato
	var y [5]int = [5]int{1, 2, 3, 4, 5}

	// Array preimpostato con grandezza devinita dal contenuto
	z := [...]int{1, 2, 3}

	println(z[1])

	// Iterare un array
	for i, val := range y {
		println("Indice:", i, ", Valore:", val)
	}
}

// Slices --> Suddividere una collezione in sotto-collezioni
func slices() {
	words := [...]string{"a", "b", "c", "d", "e", "f"}
	str1 := words[1:3]
	str2 := words[4:5]

	println(str1[0], str2[0])

	// Lunghezza e capacità di un array
	println(len(str1), cap(str1))

	// Slice preimpostato
	// Lunghezza == capacità
	slice := []int{1, 2, 3}

	println(slice[0])

	// Crea un slice o array con tipo e grandezza, valori di default a 0
	sli1 := make([]int, 10)

	// Lunghezza e capacità separate
	sli2 := make([]int, 10, 15)

	fmt.Println(sli1, sli2)

	// Lunghezza = 0
	sli3 := make([]int, 0, 5)

	// Lunghezza = 1, primo valore 100
	sli3 = append(sli3, 100)

}
