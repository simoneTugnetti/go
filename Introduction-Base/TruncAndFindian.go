// Simone Tugnetti

// Trunc And Findian

package main

import (
	"fmt"
	"strings"
)

func main() {
	trunc()
	findian()
}

// Converte un decimale in intero
func trunc() {
	println("Inserisci un numero decimale")
	var numFloat float32
	fmt.Scan(&numFloat)

	var numInt = int(numFloat)
	println("Il valore è", numInt)
}

// Trova se una parola contiene la lettera A, inizia con la I e termina con la N
func findian() {
	println("Inserisci una frase")
	var phrase string

	fmt.Scan(&phrase)
	upperPhrase := strings.ToUpper(phrase)
	if strings.Contains(upperPhrase, "A") &&
		strings.Index(upperPhrase, "I") == 0 &&
		strings.LastIndex(upperPhrase, "N") == len(upperPhrase)-1 {
		println("Found!")
	} else {
		println("Not Found!")
	}
}
