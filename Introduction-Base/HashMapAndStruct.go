// Simone Tugnetti

// Hash Map and Struct

package main

import "fmt"

func main() {
	mapImpl()
	println()
	structImpl()
}

// Maps --> Tabella Hash contenente chiave e valore
func mapImpl() {
	var idMap map[string]int

	// Creazione di una Map vuota
	idMap = make(map[string]int)

	fmt.Println(idMap)

	// Creazione di una mappa preimpostata
	newMap := map[string]int{
		"joe": 123}

	fmt.Println(newMap, newMap["joe"])

	idMap["kevin"] = 345

	fmt.Println(idMap)

	// Eliminazione di una chiave da una Map
	delete(newMap, "joe")

	fmt.Println(newMap)

	value, presence := idMap["kevin"]

	println("Valore:", value, ", è presente:", presence)

	idMap["max"] = 987

	// Iterazione di una Map
	for key, value := range idMap {
		println(key, value)
	}
}

// Struct --> Strutture dati contenente informazioni principali su un'entità
func structImpl() {
	type Person struct {
		name  string
		addr  string
		phone string
	}
	var p1 Person

	p1.name = "joe"
	p1.addr = "nowhere"

	x := p1.addr

	// Crea un puntatore struct vuoto
	p2 := new(Person)

	p3 := Person{
		name:  "alex",
		addr:  "here",
		phone: "123"}

	fmt.Println(p1, x)
	fmt.Println(p2)
	fmt.Println(p3)
}
