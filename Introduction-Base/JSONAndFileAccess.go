// Simone Tugnetti

// JSON and File Access

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	generateJSON()
	fileAccessIoutil()
	fileAccessOs()
}

func generateJSON() {
	type Person struct {
		Name string
		Age  int
		Hair string
	}

	p1 := Person{
		Name: "Simone",
		Age:  15,
		Hair: "brown"}

	// Genera un array di byte in formato JSON
	// contenente le informazioni all'interno della struct
	barr, _ := json.Marshal(p1)

	fmt.Println(string(barr))

	var p2 Person

	// Inserisce i dati all'interno dell'array nell'indirizzo della variabile p2
	// deserializzando il JSON
	json.Unmarshal(barr, &p2)

	fmt.Println(p2)

}

func fileAccessIoutil() {

	// Crea o inserisce all'interno di un file il contenuto di un array di byte
	words := []byte("Hello, World")
	ioutil.WriteFile("output.txt", words, 0777) // Permessi

	// Crea un array di byte con il contenuto di un file
	dat, _ := ioutil.ReadFile("output.txt")

	fmt.Println(string(dat))
}

func fileAccessOs() {

	// Apre il file
	f, _ := os.Open("output.txt")

	barr := make([]byte, 15)

	// Restituisce il numero di byte letti, inserendoli all'interno dell'array passato
	nb, _ := f.Read(barr)

	fmt.Println("Utilizzando OS:", string(barr))
	fmt.Println("Numero byte:", nb)

	f.Close()

	newF, _ := os.Create("osOut.txt")

	newBarr := []byte{1, 2, 3}

	newNb, _ := newF.Write(newBarr)

	newF.WriteString("\nScritta usando OS")

	fmt.Println("Numero di byte in scrittura:", newNb)

	newF.Close()

}
