// Simone Tugnetti

// Slice Excercise

package main

import (
	"fmt"
	"sort"
	"strconv"
)

// Programma che permette di inserire interi, stampando la slice ordinata,
// fino all'inserimento di una "X"
func main() {
	slices := make([]int, 3)
	var key string
	for {
		println("Inserisci un valore:")
		fmt.Scan(&key)
		if key == "X" {
			break
		}
		num, _ := strconv.Atoi(key)
		slices = append(slices, num)
		sort.Slice(slices, func(i, j int) bool {
			return slices[i] < slices[j]
		})
		fmt.Println(slices)
	}
}
