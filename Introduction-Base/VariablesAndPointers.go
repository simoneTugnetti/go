// Simone Tugnetti

// Variables And Pointers

package main

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

func main() {

	// Usa il valore di indirizzo -> Più usato
	fmt.Println("Hello World")

	// Usa il pointer dell'indirizzo della funzione
	println("Hello World")

	// Dichiarazione di variabili tipo esplicito
	var x int // init 0
	var y string

	x = 10
	y = "ciao"

	// Dichiarazione di variabile tipo implicito
	z := 16

	// Funzione new(type) inizializza un puntatore del tipo specificato
	ptr := new(int32)

	// Cambio di valore al puntatore
	*ptr = 4

	println(x, " ", y, " ", z)

	j := foo()

	println(*j)

	// Restituisce il tipo di variabile
	fmt.Println(reflect.TypeOf(x))

	first()
	constant()
	pointers()
	casting()
	strUtil()

}

// Variabile globale
var y *int

func first() {
	// Variabile locale
	x := 5
	fmt.Println(x)
}

// La funzione può non venire deallocata in quanto vi è il ritorno di un indirizzo
func foo() *int {
	x := 1
	return &x
}

// iota --> Incremento automatico lista di variabili
func constant() {

	// Crea un nuovo tipo con nome e tipologia
	type Grades int
	const (
		A Grades = iota
		B
		C
		D
		E
	)

	println("B Value: ", B)
}

func pointers() {
	var w int = 2
	var a int

	// Puntatore verso un intero
	var ip *int

	// Il puntatore prende l'indirizzo di una variabile (w)
	ip = &w

	// Il cambiamento del valore è incluso nell'indirizzo verso il puntatore
	w = 3

	// La variabile (a) prende il valore all'interno del puntatore
	a = *ip

	println(a)
}

func casting() {
	var newX int32 = 1
	var newY int16 = 2

	// Fallimento: Variabili di tipo differente
	// newX = newY

	// Successo: Casting funzionale
	newX = int32(newY)

	var firstFloat float64 = 1234.56

	fmt.Println(newX, " ", firstFloat)
}

func strUtil() {
	var firstWord = "ciao"
	var secondWord = "sono qui"

	// Comparazione di stringhe
	fmt.Println(strings.Compare(firstWord, secondWord))

	// Conversione stringa in intero
	println(strconv.Atoi(firstWord))

	// Conversione int to string
	println(strconv.Itoa(10))
}
