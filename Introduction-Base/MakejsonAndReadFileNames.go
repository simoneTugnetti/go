// Simone Tugnetti

// Make JSON And Read File Names

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	makeJSON()
	readFileNames()
}

// Crea un JSON avente nome ed indirizzo come campi
func makeJSON() {
	var name, address string

	println("Inserisci il nome:")
	fmt.Scan(&name)

	println("Inserisci l'indirizzo:")
	fmt.Scan(&address)

	newMap := map[string]string{
		"name":    name,
		"address": address}

	result, _ := json.Marshal(newMap)

	fmt.Println(string(result))
}

// Crea un file txt con una lista di nomi e cognomi per poi leggerlo
// inserendo gli stessi in una lista di slice per stamparne il contenuto
func readFileNames() {
	type Name struct {
		fname string
		lname string
	}

	slice := make([]Name, 0)
	var fileName string

	println("Inserisci il nome del file:")
	fmt.Scan(&fileName)

	names := []byte("Simone Tugnetti\nGiovanni Bietola")

	ioutil.WriteFile(fileName+".txt", names, 0777)

	dat, _ := ioutil.ReadFile(fileName + ".txt")

	fileNames := string(dat)

	fmt.Println(fileNames)

	allNames := strings.Split(fileNames, "\n")

	var firstLast []string

	for _, x := range allNames {
		firstLast = strings.Split(x, " ")
		slice = append(slice, Name{
			fname: firstLast[0],
			lname: firstLast[1]})
	}

	for i, x := range slice {
		println("First and Last n°", i)
		println(x.fname, x.lname)
	}
}
