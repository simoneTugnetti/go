// Simone Tugnetti

// Advanced Functions

package main

import "fmt"

// Variabile di tipo funzione intera con ritorno intero
var funcVar func(int) int

func incFun(x int) int {
	return x + 2
}

// Una funzione può avere a sua volta un'altra funzione passata come parametro
func argumentFun(newFun func(int) int, val int) int {
	return newFun(val) * 2
}

// Una funzione può ritornare a sua volta un'altra funzione
func addAnotherValue(val float64) func(float64) float64 {
	newVal := val * 3
	return func(subVal float64) float64 {
		return newVal + subVal
	}
}

// Variadic -> Possono essere inseriti più variabili come parametro
// convertendole all'interno della funzione come array/slice
func getMax(values ...int) int {
	max := 0
	for _, val := range values {
		if val > max {
			max = val
		}
	}
	return max
}

func main() {
	funcVar = incFun
	fmt.Println(funcVar(10))
	fmt.Println(argumentFun(incFun, 3))

	fun := argumentFun(func(x int) int {
		return x + 10
	}, 5)

	fmt.Println(fun)

	newFun := addAnotherValue(2.5)

	fmt.Println(newFun(5))
	fmt.Println(addAnotherValue(5.5)(3))

	fmt.Println(getMax(22, 34, 0))

	// Variadic -> Inserimento di una slice come lista di parametri
	vslice := []int{7, 67, 12}
	fmt.Println(getMax(vslice...))

	// Deferred -> Viene avviato un blocco o funzione al termine
	// di utilizzo del resto del programma
	defer fmt.Println("Arrivederci")
	fmt.Println("Benvenuto")

	// Verrà stampato 2, dato che il valore è rimasto in deferred prima
	// dell'incremento
	i := 1
	defer fmt.Println(i + 1)
	i++
	fmt.Println("Hello")

}
