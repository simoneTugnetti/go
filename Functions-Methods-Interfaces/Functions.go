// Simone Tugnetti

// Functions

package main

import "fmt"

func main() {
	max := getMax(10, 20)
	fmt.Println(max)

	dX, dY := doubleTwoNum(2, 3)
	fmt.Println(dX, dY)

	num := 3
	funPointer(&num)
	fmt.Println(num)

	newArray := [3]int{1, 2, 3}
	elem := getElemArray(newArray)
	fmt.Println(elem)

	editElemArray(&newArray)
	fmt.Println(newArray)

	slice := []int{2, 4, 6}

	editSlice(slice)

	fmt.Println(slice)

}

// Funzione com parametri e valore di ritorno
func getMax(a, b int) int {
	if a > b {
		return a
	}

	return b
}

// E' possibile restituire anche più di un valore
func doubleTwoNum(a, b int) (int, int) {
	return a * 2, b * 2
}

// Normalmente non avviene la modifica di un parametro al di fuori
// della funzione, ma passando l'indirizzo di un valore, questo
// risulterà modificato anche al di fuori di essa
func funPointer(x *int) {
	*x = *x + 1
}

// Funzione con array
func getElemArray(x [3]int) int {
	return x[0]
}

// Funzione con puntatore ad un array
func editElemArray(x *[3]int) {
	(*x)[0] = (*x)[0] + 5
}

// Funzione con slice, agisce come un puntatore
func editSlice(sli []int) {
	sli[0] = sli[0] + 2
}
