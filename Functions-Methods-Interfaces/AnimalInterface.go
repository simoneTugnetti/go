// Simone Tugnetti

// Animal Interface Excercise

package main

import "fmt"

var input, nameI string
var infoA = make([]infoAnimal, 0)

// Interfaccia con metodi legati al tipo di animale
type animal interface {
	Eat()
	Move()
	Speak()
}

// Informazioni sull'animale
type infoAnimal struct {
	name       string
	typeA      string
	food       string
	locomotion string
	noise      string
}

// Stampa il cibo dell'animale
func (i infoAnimal) Eat() {
	fmt.Println(i.food)
}

// Stampa il movimento dell'animale
func (i infoAnimal) Move() {
	fmt.Println(i.locomotion)
}

// Stampa il verso dell'animale
func (i infoAnimal) Speak() {
	fmt.Println(i.noise)
}

// Inserisce un animale all'interno du una lista
func newAnimal() {
	fmt.Println("Inserisci il nome dell'animale")
	fmt.Scan(&nameI)
	fmt.Println("Inserisci il tipo dell'animale")
	fmt.Scan(&input)
	for input != "cow" && input != "bird" && input != "snake" {
		fmt.Println("Inserisci il tipo dell'animale")
		fmt.Scan(&input)
	}
	switch input {
	case "bird":
		infoA = append(infoA, infoAnimal{name: nameI,
			typeA: input, food: "worms",
			locomotion: "fly", noise: "peep"})
	case "cow":
		infoA = append(infoA, infoAnimal{name: nameI,
			typeA: input, food: "grass",
			locomotion: "walk", noise: "moo"})
	case "snake":
		infoA = append(infoA, infoAnimal{name: nameI,
			typeA: input, food: "mice",
			locomotion: "slither", noise: "hsss"})
	}
	fmt.Println("Created it!")
}

// Stampa le informazioni relative ad uno specifico animale
func query() {
	fmt.Println("Inserisci il tipo dell'animale")
	fmt.Scan(&input)
	fmt.Println("Inserisci il nome dell'animale")
	fmt.Scan(&nameI)
	for _, info := range infoA {
		if nameI == info.name && input == info.typeA {
			fmt.Println("Inserisci il tipo di azione")
			fmt.Scan(&input)
			for input != "eat" && input != "move" && input != "speak" {
				fmt.Println("Inserisci il tipo di azione")
				fmt.Scan(&input)
			}
			switch input {
			case "eat":
				info.Eat()
			case "move":
				info.Move()
			case "speak":
				info.Speak()
			}
			break
		}
	}
}

func main() {
	for {
		for input != "newanimal" && input != "query" {
			fmt.Println("newanimal or query - E -> Exit")
			fmt.Scan(&input)
			if input == "E" {
				break
			}
		}
		if input == "E" {
			break
		}
		if input == "newanimal" {
			newAnimal()
		} else {
			query()
		}
	}
}
