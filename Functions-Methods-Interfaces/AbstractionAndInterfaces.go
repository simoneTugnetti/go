// Simone Tugnetti

// Abstraction and Interfaces

package main

import "fmt"

// Interfaccia -> Struttura dati astratta con metodi da implementare/riscrivere
// Possono avere sia tipo che valori dinamici
type shape2D interface {
	Area() float64
	Perimetro() float64
}

// Interfaccia con funzione senza ritorno
type speak interface {
	verify()
}

type dog struct {
	name string
}

type triangolo struct {
	lato    float64
	base    float64
	altezza float64
}

type rettangolo struct {
	base    float64
	altezza float64
}

func (t triangolo) Area() float64 {
	return (t.base * t.altezza) / 2
}

func (t triangolo) Perimetro() float64 {
	return t.lato * 3
}

func (d *dog) verify() {
	if d == nil {
		fmt.Println("Il cane non esiste")
	} else {
		fmt.Println("Il cane si chaiama", d.name)
	}
}

func (r rettangolo) Area() float64 {
	return r.base * r.altezza
}

func (r rettangolo) Perimetro() float64 {
	return (r.base + r.altezza) * 2
}

// E' possibile passare un'interfaccia come parametro con con dati già impostati
func fitInYard(s shape2D) bool {
	if s.Area() > 100 && s.Perimetro() > 100 {
		return true
	}
	return false
}

// E' possibile verificare il tipo al quale può essere associata un'interfaccia
func verifyConcreteType(s shape2D) {
	_, okt := s.(triangolo)
	if okt {
		fmt.Println("La forma è un triangolo")
	}
	// rect -> tipo associato all'interfaccia
	// okr -> true o false in caso in cui l'interfaccia abbia un tipo concreto
	rect, okr := s.(rettangolo)
	if okr {
		fmt.Println("La forma è un rettangolo")
		fmt.Println("Area:", rect.Area())
	}
}

func verifyConcreteTypeSwitch(s shape2D) {
	switch sh := s.(type) {
	case triangolo:
		fmt.Println("La forma è un triangolo avente lato", sh.lato)
	case rettangolo:
		fmt.Println("La forma è un rettangolo")
	}
}

// Passando un'interfaccia vuota, viene data la possibilità di passare un qualsiasi tipo
func printMe(val interface{}) {
	fmt.Println(val)
}

func main() {
	var shape shape2D
	var tr = triangolo{lato: 2.5, base: 5, altezza: 5}

	// Se presenti, i metodi vengono convertiti agli stessi presenti all'interno dell'interfaccia
	shape = tr
	fmt.Println(shape.Area())
	fmt.Println(shape.Perimetro())
	verifyConcreteTypeSwitch(shape)

	var rett = rettangolo{base: 8, altezza: 4.5}
	shape = rett
	fmt.Println(shape.Area())
	fmt.Println(shape.Perimetro())

	fmt.Println(fitInYard(shape))

	// Un'interfaccia può avere valore dinamico nullo
	var s1 speak
	var d *dog
	s1 = d
	s1.verify()

	// In questo modo viene passato l'indirizzo ad un oggetto non nullo
	var d2 = dog{name: "Gianni"}
	s1 = &d2
	s1.verify()

	printMe(shape)
	printMe(155)
	verifyConcreteType(shape)
}
