// Simone Tugnetti

// Displacement Excercise

package main

import (
	"fmt"
	"math"
)

// Funzione che determina il risultato di tale formula -> s = ½ a t^2 + vo t + so
func genDisplace(accel float64, initVel float64, initDispl float64) func(float64) float64 {
	return func(time float64) float64 {
		return (0.5 * accel * math.Pow(time, 2)) + (initVel * time) + initDispl
	}
}

func main() {
	fun := genDisplace(2.3, 4, 1.2)
	fmt.Println(fun(3))
	fmt.Println(fun(5))
}
