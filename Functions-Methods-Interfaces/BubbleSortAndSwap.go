// Simone Tugnetti

// Bubble Sort And Swap

package main

import (
	"fmt"
)

func main() {
	var slice []int
	var val int
	for i := 0; i < 5; i++ {
		println("Inserisci un valore")
		fmt.Scan(&val)
		slice = append(slice, val)
	}
	bubbleSort(slice)
	fmt.Println(slice)

	var position int
	println("Scegli una posizione")
	fmt.Scan(&position)
	for position >= len(slice)-1 || position < 0 {
		println("Posizione non valida, riprova")
		fmt.Scan(&position)
	}
	swap(slice, position)
	fmt.Println(slice)

}

// Esegue l'ordinamento di una slice con metodo Bubble Sort
func bubbleSort(sli []int) {
	var temp int
	for i := 0; i < len(sli); i++ {
		for j := i + 1; j < len(sli); j++ {
			if sli[i] > sli[j] {
				temp = sli[i]
				sli[i] = sli[j]
				sli[j] = temp
			}
		}
	}
}

// Scambia le due posizioni all'interno della slice
func swap(sli []int, position int) {
	var temp int
	temp = sli[position]
	sli[position] = sli[position+1]
	sli[position+1] = temp
}
