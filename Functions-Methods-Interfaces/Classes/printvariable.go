// Simone Tugnetti

// Custom Package Example

package printvariable

import "fmt"

var x int = 5

// PrintX stampa di una variabile
func PrintX() {
	fmt.Println(x)
}

// Funzione Privata -> Iniziando in lower-case, la funzione risulta accessibile
// solo al package corrente
func printAnotherX() {
	fmt.Println(x + 1)
}
