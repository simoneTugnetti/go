// Simone Tugnetti

// Point

package point

import "fmt"

// Struct for data
type Point struct {
	x float64
	y float64
}

// Init -> Metodo "Costruttore", inizializza x e y
// L'oggetto è definito come puntatore, ciò significa
// che i valori al proprio interno verranno modificati all'esteno di esso
// e non tramite inizializzazione diretta, cioè non inizializzando una struct
// Point dall'esterno tramite dichiarazione
func (p *Point) Init(x, y float64) {
	// conversione automatica ad un non riferimento
	p.x = x
	p.y = y
}

// Scale -> Aumenta o diminuisce le proporzioni in scala
func (p *Point) Scale(v float64) {
	p.x = p.x * v
	p.y = p.y * v
}

// PrintXY -> Stampa x e y
func (p Point) PrintXY() {
	fmt.Println(p.x, p.y)
}
