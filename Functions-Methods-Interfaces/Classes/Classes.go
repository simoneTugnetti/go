// Simone Tugnetti

// Classes

package main

import (
	"fmt"
	"math"
	"point"
	"printvariable"
)

// In GO non esiste una keyword per definire una classe, esiste però l'associare un dato
// ad un determinato metodo, esattamente come per le classi
type myData int

type Point struct {
	x float64
	y float64
}

// Le funzioni diventano così dei metodi, in grado di associare un'operazione alla classe (oggetto)
// al quale è associata
func (md myData) double() int {
	return int(md * 2)
}

// Metodo che calcola la distanza di due punti presi come struct receiver type
// In questo modo Point è l'oggetto, x e y le variabili e distToOrigin è il metodo
func (p Point) distToOrigin() float64 {
	return math.Sqrt(math.Pow(p.x, 2) + math.Pow(p.y, 2))
}

func main() {

	// La variabile di parametro in questo caso è implicita
	data := myData(3)
	fmt.Println(data.double())

	pointVar := Point{x: 12, y: 20}
	fmt.Println(pointVar.distToOrigin())

	// E' possibile importare un pacchetto con funzioni/metodi al suo interno
	printvariable.PrintX()

	var point2 point.Point
	point2.Init(23, 55)
	point2.Scale(3)
	point2.PrintXY()
}
