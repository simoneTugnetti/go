// Simone Tugnetti

// Animals Excercise

package main

import "fmt"

// Struct Animal per contenere i dati del cibo, movimento e verso
type animal struct {
	food       string
	locomotion string
	noise      string
}

// Costruttore dell'oggetto animal
func (a *animal) init(food, locomotion, noise string) {
	a.food = food
	a.locomotion = locomotion
	a.noise = noise
}

// Stampa del cibo
func (a animal) eat() {
	fmt.Println(a.food)
}

// Stampa del movimento
func (a animal) move() {
	fmt.Println(a.locomotion)
}

// Stampa del verso
func (a animal) speak() {
	fmt.Println(a.noise)
}

func main() {
	var animalVar animal
	var input string
	for {
		fmt.Println("Inserisci il nome dell'animale - E -> Exit")
		fmt.Scan(&input)
		if input == "E" {
			break
		}
		switch input {
		case "cow":
			animalVar.init("grass", "walk", "moo")
		case "bird":
			animalVar.init("worms", "fly", "peep")
		case "snake":
			animalVar.init("mice", "slither", "hsss")
		}
		fmt.Println("Inserisci il tipo di azione")
		fmt.Scan(&input)
		switch input {
		case "eat":
			animalVar.eat()
		case "move":
			animalVar.move()
		case "speak":
			animalVar.speak()
		}
	}
}
